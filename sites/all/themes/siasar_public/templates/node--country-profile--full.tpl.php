<?php
//kpr(get_defined_vars());
//http://drupalcontrib.org/api/drupal/drupal--modules--node--node.tpl.php
//node--[CONTENT TYPE].tpl.php

if ($classes) {
  $classes = ' class="'. $classes . ' "';
}

if ($id_node) {
  $id_node = ' id="'. $id_node . '"';
}

hide($content['comments']);
hide($content['links']);
hide($content['field_main_photo']);
hide($content['field_colaboradores']);
hide($content['group_contact']);
hide($content['group_reports']);
?>

<!-- node.tpl.php -->
<article <?php print $id_node . $classes .  $attributes; ?> role="article">
  <?php print $mothership_poorthemers_helper; ?>

  <?php print $header; ?>

  <div class="content">
    <?php print render($content);?>

    <iframe src="<?php print $dashboard_url; ?>" scrolling="no" noresize="noresize"></iframe>

    <footer>
      <?php print render($content['group_reports']); ?>
      <?php print render($content['group_contact']); ?>
      <?php print render($content['field_colaboradores']); ?>
    </footer>

  </div>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>
</article>
