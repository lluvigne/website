(function ($) {
  'use strict';

  $(document).ready(function () {

    var selector = '.field-com-report-links, .field-com-download-links';

    $(selector).createDownloadGridOverlay();

  });

})(jQuery);


(function ($) {
  'use strict';

  $.fn.createDownloadGridOverlay = function () {
    var $body = $('body');

    this.on('click', createGridOverlay);


    function createGridOverlay(e) {
      e.preventDefault();

      var $overlaySource = $(e.currentTarget);
      var closeHtml = '<span class="button-close" />';
      var $content = $overlaySource.find('ul').clone();
      var $topOverlay, $closeButton;

      $body.prepend('<div class="top-overlay"></div>');
      $topOverlay = $('.top-overlay');
      $topOverlay.append(closeHtml);
      $topOverlay.append($content);


      $closeButton = $topOverlay.find('.button-close');
      $closeButton.on('click', function () {
        $topOverlay.fadeOut('fast', function () {
          $(this).remove();
        });
      });
    }
  };

}(jQuery));
