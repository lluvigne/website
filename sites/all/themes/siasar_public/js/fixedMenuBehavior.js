// Behaviors related to the top header menu being fixed.
//
// 1 - Shrink on scroll down.


(function ($) {
  'use strict';

  $(document).ready(function () {
    var headerMenu = document.querySelector('header[role=banner');
    var scrolledDown = false;
    var scrollY, ticking;

    window.addEventListener('scroll', function (e) {

      scrollY = window.scrollY;

      if (!ticking) {
        window.requestAnimationFrame(function () {
          shrinkOrGrow();
          ticking = false;
        });
        ticking = true;
      }

    });

    function shrinkOrGrow() {
      if (scrollY == 0 && scrolledDown == true) {
        headerMenu.classList.remove('scrolled-down');
        scrolledDown = false;
      }
      if (scrollY > 60 && scrolledDown == false) {
        headerMenu.classList.add('scrolled-down');
        scrolledDown = true;
      }
    }
  });
})(jQuery);
