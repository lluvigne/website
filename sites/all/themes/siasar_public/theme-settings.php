<?php

function siasar_public_form_system_theme_settings_alter(&$form, $form_state) {
  $form['dashboard_url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Dashboard URL'),
    '#default_value' => theme_get_setting('dashboard_url'),
    '#description'   => t("Dashboard URL. Include protocol (http or https), but don't include the trailing slash. Example: <em>http://dashboard.siasar.org</em>"),
  );
}
