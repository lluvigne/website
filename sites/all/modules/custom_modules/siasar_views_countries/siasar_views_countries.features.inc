<?php
/**
 * @file
 * siasar_views_countries.features.inc
 */

/**
 * Implements hook_views_api().
 */
function siasar_views_countries_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
